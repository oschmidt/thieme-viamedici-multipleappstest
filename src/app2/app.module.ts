import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { WindowRef } from './windowRef.service';


import { AppComponent }  from './app.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ],
  providers: [
   WindowRef
  ],
})
export class AppModule2 { }
