import { Component } from '@angular/core';
import { WindowRef } from './windowRef.service';

@Component({
  selector: 'app2',
  templateUrl: './app.component.html',
})
export class AppComponent  { 
  name = 'Angular-App2'; 

  constructor(private winRef: WindowRef) {
    winRef.nativeWindow.EventEmitter.dispatch('appshell');
    winRef.nativeWindow.EventEmitter.subscribe('learnModuleApp', function(data: any) {
      console.log("Hello from Angular2-App");
      console.dir(data);
    });
  }

  ngOnInit() {
  
  }
}
