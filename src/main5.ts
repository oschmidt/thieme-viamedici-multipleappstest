import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app5/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
