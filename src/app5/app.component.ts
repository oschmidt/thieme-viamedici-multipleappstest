import { Component } from '@angular/core';
import { WindowRef } from './windowRef.service';

@Component({
  selector: 'app5',
  templateUrl: './app.component.html',
})
export class AppComponent  {
  name = 'Angular-App5';

  constructor(private winRef: WindowRef) {}

  ngOnInit() {
    this.winRef.nativeWindow.EventEmitter.subscribe('App5', function(data: any) {
      console.log("Hello from Angular-App");
      console.dir(data);
      console.log(name);
      //this.name = "data.test";
    });
    this.winRef.nativeWindow.EventEmitter.dispatch('appshell');
  }
}
