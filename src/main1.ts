import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


import { AppModule } from './app/app.module';


platformBrowserDynamic().bootstrapModule(AppModule)
    .then((ref) => {
      setTimeout(()=> {
        ref.destroy();
      }, 30000);
    })
  .catch(err => console.error(err));

